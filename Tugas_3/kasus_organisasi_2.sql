show databases;
use sdm;
show tables;

--CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO

SELECT nama FROM employee WHERE atasan_id is NULL;

--Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa

SELECT nama FROM employee WHERE id NOT IN (SELECT DISTINCT atasan_id FROM employee WHERE atasan_id IS NOT NULL);

--Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur

SELECT nama FROM employee WHERE atasan_id=1;

--Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager

SELECT nama FROM employee WHERE atasan_id IN (SELECT DISTINCT id FROM employee WHERE atasan_id=1);

--Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama.

SELECT COUNT(id) FROM employee WHERE atasan_id IN (SELECT id FROM employee WHERE nama='Pak Budi');